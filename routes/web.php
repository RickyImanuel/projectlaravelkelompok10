<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BookController@index');
Route::get('/book/detail/{id}', 'BookController@show');
Route::get('/book/manage', 'BookController@index1');
Route::post('/book/create', 'BookController@store');
Route::get('/book/create', 'BookController@create');
Route::get('/book/manage/update/{id}', 'BookController@edit');
Route::Post('/book/manage/update/{id}', 'BookController@update');
Route::get('/book/manage/delete/{id}', 'BookController@destroy');


Route::get('/users', 'UserController@index');
Route::get('/users/detail/{id}', 'UserController@show');
Route::get('/users/update/{id}', 'UserController@edit');
Route::Post('/users/update/{id}', 'UserController@update');
Route::get('/users/delete/{id}', 'UserController@destroy');

Route::get('/transaction', 'TransactionController@index');

Route::get('/cart', 'CartController@index');

Route::get('/genre/manage', 'GenreController@index');
Route::post('/genre/manage', 'GenreController@store');
Route::get('/genre/manage/update/{id}', 'GenreController@edit');
Route::post('/genre/manage/update', 'GenreController@update');
Route::post('/genre/manage/delete', 'GenreController@destroy');


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
