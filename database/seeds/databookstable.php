<?php

use Illuminate\Database\Seeder;

class databookstable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $json = \Illuminate\Support\Facades\File::get(__DIR__ . '/data/databooks.json');
        $books = json_decode($json);

        foreach($books as $book){
            \Illuminate\Support\Facades\DB::table('databooks')->insert([
                'jumlahbuku' => $book->jumlahbuku
            ]);
        }
    }
}
