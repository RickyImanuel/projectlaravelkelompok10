<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = \Illuminate\Support\Facades\File::get(__DIR__ . '/data/genres.json');
        $genres = json_decode($json);

        foreach ($genres as $genre) {
            \Illuminate\Support\Facades\DB::table('genres')->insert([
                'id' => \Illuminate\Support\Str::uuid(),
                'name' => $genre->name
            ]);
        }
    }
}
