<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $json = \Illuminate\Support\Facades\File::get(__DIR__ . '/data/users.json');
        $users = json_decode($json);

        $faker = \Faker\Factory::create('id_ID');

        foreach ($users as $user) {
            $split = explode(' ', $user->name);
            $firstname = $split[0];
            $lowercase = strtolower($firstname);

            \Illuminate\Support\Facades\DB::table('users')
                ->insert([
                   
                    'initial' => $user->initial,
                    'name' => $user->name,
                    'email' => "$lowercase@email.com",
                    'password' => $user->password,
                    'phone_number' => $faker->bothify('###-##-###'),
                    'address'=>$faker->address,
                    'status' => 'Active',
                ]);
        }
    }
}
