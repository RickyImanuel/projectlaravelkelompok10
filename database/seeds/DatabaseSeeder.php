<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            GenresTableSeeder::class,
            // databookstable::class,
            BooksTableSeeder::class,
            BookDetailsTableSeeder::class,

        ]);
    }
}
