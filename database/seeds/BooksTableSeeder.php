<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = \Illuminate\Support\Facades\File::get(__DIR__ . '/data/books.json');
        $books = json_decode($json);

        $faker = \Faker\Factory::create('id_ID');

        foreach ($books as $book) {
            \Illuminate\Support\Facades\DB::table('books')->insert([
                'id' => \Illuminate\Support\Str::uuid(),
                'name' => $book->name,
                'author' => $book->author,
                'synopsis' => $book->synopsis,
                'price' => $faker->numberBetween(500, 2000) * 100,
                // 'databooks_id'=>$book->databooks_id
            ]);
        }
    }
}
