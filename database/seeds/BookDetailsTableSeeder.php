<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = DB::table('books')->select(['id', 'name'])->get();
        $genres = DB::table('genres')->select(['id', 'name'])->get();

        foreach ($books as $book) {
            $this->insert($book, "Harry Potter series", ["Drama", "Mystery", "Thriller"], $genres);
            $this->insert($book, "The Hunger Games series", ["Drama", "Science Fiction"], $genres);
            $this->insert($book, "To Kill a Mockingbird", ["Coming-of-age"], $genres);
            $this->insert($book, "The Fault in Our Stars", ["Young-adult fiction"], $genres);
            $this->insert($book, "The Hobbit", ["Fantasy"], $genres);
            $this->insert($book, "The Catcher in the Rye", ["Coming-of-age"], $genres);
            $this->insert($book, "The Lord of the Rings", ["Fantasy"], $genres);
            $this->insert($book, "Fahrenheit 451", ["Fiction"], $genres);
            $this->insert($book, "Looking for Alaska", ["Young-adult fiction"], $genres);
            $this->insert($book, "The Book Thief", ["Young-adult fiction"], $genres);
            $this->insert($book, "The Giver series", ["Young-adult fiction"], $genres);
            $this->insert($book, "The Hitchhiker's Guide to the Galaxy series", ["Science fiction"], $genres);
            $this->insert($book, "The Outsiders", ["Young-adult fiction"], $genres);
            $this->insert($book, "Anne of Green Gables series", ["Novel", "Fiction"], $genres);
            $this->insert($book, "His Dark Materials series", ["Mystery", "Action and adventure", "Fantasy"], $genres);
            $this->insert($book, "The Perks of Being a Wallflower", ["Novel", "Young-adult fiction"], $genres);
            $this->insert($book, "The Princess Bride", ["Romance", "Fantasy", "Action and adventure"], $genres);
            $this->insert($book, "Lord of the Flies", ["Novel", "Young-adult fiction"], $genres);
            $this->insert($book, "Divergent series", ["Action and adventure", "Mystery", "Thriller", "Science fiction"], $genres);
            $this->insert($book, "Paper Towns", ["Young-adult fiction", "Mystery"], $genres);
            $this->insert($book, "The Mortal Instruments series", ["Young-adult fiction", "Fantasy"], $genres);
            $this->insert($book, "An Abundance of Katherines", ["Novel", "Young-adult fiction"], $genres);
            $this->insert($book, "Flowers For Algernon", ["Novel", "Science fiction"], $genres);
            $this->insert($book, "Thirteen Reasons Why", ["Drama", "Thriller", "Mystery"], $genres);
            $this->insert($book, "The Curious Incident of the Dog in the Night-Time", ["Novel", "Mystery", "Crime", "Fiction"], $genres);
        }
    }

    private function insert($book, $name, $filter, $genres)
    {
        if ($book->name === $name) {
            $bookGenres = $genres->whereIn('name', $filter);
            foreach ($bookGenres as $bg) {
                DB::table('book_details')->insert([
                    'book_id' => $book->id,
                    'genre_id' => $bg->id
                ]);
            }
        }
    }
}
