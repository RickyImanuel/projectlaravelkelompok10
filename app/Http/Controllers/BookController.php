<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(session('success_message')){
            Alert::success('Success Create', 'Success!');
        }
        $books = DB::table('books')
            ->select('*')
            ->orderBy('name')
            ->get();
        
        return view('home', compact('books'));
    }

    public function index1()
    {
        $books = DB::table('books')
            ->select('*')
            ->orderBy('name')
            ->get();
        return view('books.manage', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create');
    }

  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);
        DB::table('books')->insert([
            'id' => \Illuminate\Support\Str::uuid(),
            'name' => $request->book_name,
            'author' => $request->book_author,
            'synopsis' => $request->book_synopsis,
            'price' => $request->book_price,
            'genres' => $request->book_genre,
        ]);

        return redirect('/')->withSuccessMessage('Sucessfully added book');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = DB::table('books')
            ->where('books.id', '=', $id)
            ->first();

        $genres = DB::table('genres')
            ->join('book_details', 'genres.id', '=', 'book_details.genre_id')
            ->join('books', 'book_details.book_id', '=', 'books.id')
            ->where('books.id', '=', $id)
            ->orderBy('genres.name')
            ->get('genres.name');

            $genres1=DB::table('books')->select('genres')->where('id','=',$id)->first();
        return view('books.show', compact('book', 'genres','genres1'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = DB::table('books')
                ->where('id', '=', $id)
                ->first();
    
            return view('books.edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('books')
            ->where('id', '=', $request->book_id)
            ->update([
                'name' => $request->book_name,
                'author' => $request->book_author,
                'synopsis' => $request->book_synopsis,
                'price' => $request->book_price,
                'genres' => $request->book_genre,
            ]);

        DB::table('book_details')
        ->where('book_id','=',$id)
        ->delete();

        return redirect('/book/manage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('books')
        ->where('id', '=', $id)
        ->delete();

        DB::table('book_details')
        ->where('book_id','=',$id)
        ->delete();
    return redirect()->back();
    }
}
