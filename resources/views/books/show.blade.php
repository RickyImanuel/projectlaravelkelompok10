@extends('layouts.app')

@section('title', $book->name)

@section('content')
    <div class="row">
        <div class="col">Title</div>
        <div class="col">{{ $book->name }}</div>
    </div>

    <br>

    <div class="row">
        <div class="col">Author</div>
        <div class="col">{{ $book->author }}</div>
    </div>

    <br>

    <div class="row">
        <div class="col">Price</div>
        <div class="col">IDR {{ $book->price }}</div>
    </div>

    <br>

    <div class="row">
        <div class="col">Synopsis</div>
        <div class="col">{{ $book->synopsis }}</div>
    </div>

    <br>

    <br>

    <div class="row">
        <div class="col">Genres</div>
        <div class="col">
            @foreach ($genres as $genre)
                @if (!$loop->last)
                    {{ $genre->name . ', ' }}
                @else
                    {{ $genre->name }}
                @endif
            @endforeach
           {{$genres1->genres}}
        </div>
    </div>
@endsection
