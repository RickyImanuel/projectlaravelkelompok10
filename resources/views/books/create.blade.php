@extends('layouts.app')

@section('title', 'Update books')

@section('content')
    <form action="/book/create" method="POST">
        @csrf
        

        <div class="form-group row">
            <label for="book_name" class="col-5 col-form-label">Name</label>
            <div class="col-7">
                <input type="text" class="form-control" name="book_name" id="book_name" 
                       required>
            </div>
        </div>
        <div class="form-group row">
            <label for="book_author" class="col-5 col-form-label">Author</label>
            <div class="col-7">
                <input type="text" class="form-control" name="book_author" id="book_author" 
                       required>
            </div>
        </div>
        <div class="form-group row">
            <label for="book_synopsis" class="col-5 col-form-label">synopsis</label>
            <div class="col-7">
                <input type="text" class="form-control" name="book_synopsis" id="book_synopsis"
                       required>
            </div>
        </div>
        <div class="form-group row">
            <label for="book_price" class="col-5 col-form-label">price</label>
            <div class="col-7">
                <input type="text" class="form-control" name="book_price" id="book_price" 
                       required>
            </div>
        </div>
        <div class="form-group row">
            <label for="book_genre" class="col-5 col-form-label">genre</label>
            <div class="col-7">
                <input type="text" class="form-control" name="book_genre" id="book_genre"
                       required>
            </div>
        </div>


        <button type="submit" class="btn btn-primary d-block mx-auto">Create</button>
    </form>
@endsection
