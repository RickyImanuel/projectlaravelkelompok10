@extends('layouts.app')

@section('title', 'Manage Book')

@section('content')
    @component('components.card')
        <h1 class="text-center">Insert Book</h1>

        <br>

        <td><a href="/book/create" class="btn btn-primary">create</a></td>
    @endcomponent

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($books as $book)
                <tr>
                    <td>{{ $book->name }}</td>
                    <td class="d-flex">
                        <a href="/book/manage/update/{{ $book->id }}" class="btn btn-primary">update</a>

                        <div class="mx-2"></div>

                        <a href="/book/manage/delete/{{$book->id}}" class="btn btn-danger">delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
