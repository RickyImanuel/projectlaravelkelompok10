@extends('layouts.app')

@section('title', 'Update books')

@section('content')
    <form action="/book/manage/update/{{$book->id}}" method="POST">
        @csrf
        <input type="hidden" name="book_id" value="{{ $book->id }}">

        <div class="form-group row">
            <label for="book_name" class="col-5 col-form-label">Name</label>
            <div class="col-7">
                <input type="text" class="form-control" name="book_name" id="book_name" value="{{ $book->name }}"
                       required>
            </div>
        </div>
        <div class="form-group row">
            <label for="book_author" class="col-5 col-form-label">Author</label>
            <div class="col-7">
                <input type="text" class="form-control" name="book_author" id="book_author" value="{{ $book->author }}"
                       required>
            </div>
        </div>
        <div class="form-group row">
            <label for="book_synopsis" class="col-5 col-form-label">synopsis</label>
            <div class="col-7">
                <input type="text" class="form-control" name="book_synopsis" id="book_synopsis" value="{{ $book->synopsis }}"
                       required>
            </div>
        </div>
        <div class="form-group row">
            <label for="book_price" class="col-5 col-form-label">price</label>
            <div class="col-7">
                <input type="text" class="form-control" name="book_price" id="book_price" value="{{ $book->price }}"
                       required>
            </div>
        </div>
        <div class="form-group row">
            <label for="book_genre" class="col-5 col-form-label">genre</label>
            <div class="col-7">
                <input type="text" class="form-control" name="book_genre" id="book_genre" value="{{ $book->genres}}"
                       required>
            </div>
        </div>

        <button type="submit" class="btn btn-primary d-block mx-auto">Update</button>
    </form>
@endsection
