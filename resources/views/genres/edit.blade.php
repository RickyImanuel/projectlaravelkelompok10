@extends('layouts.app')

@section('title', 'Update genre')

@section('content')
    <form action="/genre/manage/update" method="POST">
        @csrf

        <input type="hidden" name="genre_id" value="{{ $genre->id }}">

        <div class="form-group row">
            <label for="genre_name" class="col-5 col-form-label">Name</label>
            <div class="col-7">
                <input type="text" class="form-control" name="genre_name" id="genre_name" value="{{ $genre->name }}"
                       required>
            </div>
        </div>
        <button type="submit" class="btn btn-primary d-block mx-auto">Update</button>
    </form>
@endsection
