@extends('layouts.app')

@section('title', 'Manage genre')

@section('content')
    @component('components.card')
        <h1 class="text-center">Insert genre</h1>

        <br>

        <form action="/genre/manage" method="POST">
            @csrf

            <div class="form-group row">
                <label for="genre_name" class="col-5 col-form-label">Name</label>
                <div class="col-7">
                    <input type="text" class="form-control" name="genre_name" id="genre_name" required>
                </div>
            </div>

            <button type="submit" class="btn btn-primary d-block mx-auto">Insert</button>
        </form>
    @endcomponent

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($genres as $genre)
                <tr>
                    <td>{{ $genre->name }}</td>
                    <td class="d-flex">
                        <a href="/genre/manage/update/{{ $genre->id }}" class="btn btn-primary">Update</a>

                        <div class="mx-2"></div>

                        <form action="/genre/manage/delete" method="POST">
                            @csrf
                            <input type="hidden" name="id" value="{{ $genre->id }}">
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
