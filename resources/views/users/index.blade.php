@extends('layouts.app')

@section('title', 'Manage user')

@section('content')

    <div class="table-responsive" style="font-size: 14px">
        <table class="table">
            <thead>
            <tr>
                <th>Initial</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Address</th>
                <!-- <th>Status</th> -->
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->initial }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->phone_number }}</td>
                    <td>{{ $user->address }}</td>
                    <!-- <td>{{ $user->status }}</td> -->
                    <td><a href="/users/detail/{{ $user->id }}" class="btn btn-info">View Detail</a></td>
                    <td><a href="/users/update/{{ $user->id }}" class="btn btn-primary">Update</a></td>
                    <td><a href="/users/delete/{{ $user->id }}" class="btn btn-danger">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

