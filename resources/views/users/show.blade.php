@extends('layout')

@section('title', 'User Detail')

@section('content')

    @component('components.card')
        <h1 class="text-center">User Detail</h1>

        <br>

        <div class="row">
            <div class="col">Initial</div>
            <div class="col">{{ $user->initial }}</div>
        </div>

        <br>

        <div class="row">
            <div class="col">Name</div>
            <div class="col">{{ $user->name }}</div>
        </div>

        <br>

        <div class="row">
            <div class="col">Email Address</div>
            <div class="col">{{ $user->email }}</div>
        </div>

        <br>

        <div class="row">
            <div class="col">Phone Number</div>
            <div class="col">{{ $user->phone_number }}</div>
        </div>

        <br>

        <div class="row">
            <div class="col">Address</div>
            <div class="col">{{ $user->address }}</div>
        </div>

        <br>

        <div class="row">
            <div class="col">Status</div>
            <div class="col">{{ $user->status }}</div>
        </div>
    @endcomponent

@endsection
