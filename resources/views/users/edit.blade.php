@extends('layouts.app')

@section('title', 'Update genre')

@section('content')
    <form action="/users/update/{{$user->id}}" method="POST">
        @csrf

        <input type="hidden" name="genre_id" value="{{ $user->id }}">

        <div class="form-group row">
            <label for="user_name" class="col-5 col-form-label">Name</label>
            <div class="col-7">
                <input type="text" class="form-control" name="user_name" id="user_name" value="{{ $user->name }}"
                       required>
            </div>
        </div>

        <div class="form-group row">
            <label for="user_initial" class="col-5 col-form-label">Initial</label>
            <div class="col-7">
                <input type="text" class="form-control" name="user_initial" id="user_initial" value="{{ $user->initial }}"
                       required>
            </div>
        </div>

        <div class="form-group row">
            <label for="user_email" class="col-5 col-form-label">Email</label>
            <div class="col-7">
                <input type="text" class="form-control" name="user_email" id="user_email" value="{{ $user->email }}"
                       required>
            </div>
        </div>

        <div class="form-group row">
            <label for="user_number" class="col-5 col-form-label">PhoneNumber</label>
            <div class="col-7">
                <input type="text" class="form-control" name="user_number" id="user_name" value="{{ $user->phone_number }}"
                       required>
            </div>
        </div>

        <div class="form-group row">
            <label for="user_address" class="col-5 col-form-label">Address</label>
            <div class="col-7">
                <input type="text" class="form-control" name="user_address" id="user_address" value="{{ $user->address }}"
                       required>
            </div>
        </div>

  
        <button type="submit" class="btn btn-primary d-block mx-auto">Update</button>
    </form>
@endsection