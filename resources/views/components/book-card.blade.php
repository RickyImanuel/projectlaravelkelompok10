<div class="card">
    <div class="card-body">
        <div class="d-flex flex-column justify-content-between" style="height: 300px;">
            <h5 class="card-title">{{ $name }}</h5>
            <div class="card-text" style="font-size: 12px">
                <p>
                    By: {{ $author }}
                    <br>
                    IDR {{ $price }}
                </p>

                <br>

                <a href="/book/detail/{{ $id }}" class="card-link">View Details ></a>
            </div>
        </div>
    </div>
</div>
