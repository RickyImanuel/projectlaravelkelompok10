<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>

<nav class="navbar d-flex justify-content-between py-lg-2 px-lg-4" style="background-color: #281e5a">
    <a href="/" class="navbar-brand text-white">graBeDia</a>

    <ul class="nav">
        <li class="nav-item"><a class="nav-link text-white" href="/book/manage">Manage Books</a></li>
        <li class="nav-item"><a class="nav-link text-white" href="/genre/manage">Manage Genres</a></li>
        <li class="nav-item"><a class="nav-link text-white" href="/users">View Users</a></li>
    </ul>
</nav>

<main class="container mt-5">
    @yield('content')
</main>

</body>
</html>
