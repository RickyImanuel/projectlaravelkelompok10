@extends('layouts.app')

@section('content')
<div class="d-flex flex-wrap">
        @foreach ($books as $book)
            <div class="m-2" style="width: 200px">
                @component('components.book-card')
                    @slot('name') {{ $book->name }} @endslot
                    @slot('author') {{ $book->author }} @endslot
                    @slot('price') {{ $book->price }} @endslot
                    @slot('id') {{ $book->id }} @endslot
                @endcomponent
            </div>
        @endforeach
    </div>
@endsection
